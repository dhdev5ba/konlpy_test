import argparse

import numpy as np
import requests
from bs4 import BeautifulSoup
import re
from konlpy.tag import Mecab


mecab = Mecab()


p = argparse.ArgumentParser()

p.add_argument('--url', type=str, required=True)
p.add_argument('--count', type=int, default=5)

config = p.parse_args()


def find_tags(url, count):
    if url is not None:
        try:
            headers = {
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"
            }
            res = requests.get(url, headers=headers)
            res = res.content

            soup = BeautifulSoup(res, 'html.parser')
            # Basic
            soup_1 = [h.string.strip() if h.string else '' for h in soup.find_all('h1')]
            soup_2 = [h.string.strip() if h.string else '' for h in soup.find_all('h3')]
            soup_3 = [p.string.strip() if p.string else '' for p in soup.find_all('p')]

            # Some domain need specific headers
            try:
                # Naver
                naver = [soup.find('div', id="articleBodyContents").text]
            except:
                naver = []

            soup = soup_1 + soup_2 + soup_3 + naver
            # Concatenate to a string
            soup = ' '.join(soup).strip()

            # Remove noise str
            removal_list = "‘, ’, ◇, ‘, ”,  ’, ', ·, \“, ·, △, ●,  , ■, (, ), \", >>, `, /, -,∼,=,ㆍ<,>, .,?, !,【,】, …, ◆,%"
            soup = re.sub("[.,\'\"’‘”“!?]", "", soup)
            soup = re.sub("[^가-힣0-9a-zA-Z\\s]", " ", soup)
            soup = re.sub("\s+", " ", soup)
            soup = soup.translate(str.maketrans(removal_list, ' '*len(removal_list)))
            soup = soup.strip()
            print(soup)


            if soup is not None:
                soup = mecab.nouns(soup)
                result = dict()
                for item in soup:
                    if result.get(item):
                        result[item] += 1
                    else:
                        result[item] = 1
                
                result = [(k, v) for k, v in result.items() if len(k) > 1]
                if len(result) != 0:
                    result.sort(reverse=True, key=lambda x: x[1])
                    result = [k for k, _ in result]
                    print(result[:int(count)])
                    return result[:int(count)]
        except:
            print('None')
            return 'None'
    print('None')
    return 'None'


find_tags(config.url, config.count)